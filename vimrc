set nocompatible

set history=1000

set showcmd
set showmode
set number

set incsearch
set hlsearch
set showmatch

set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
set smartindent
set scrolloff=5

filetype plugin on
filetype indent on

set fileformats=unix
set ff=unix

"turn on syntax highlighting
syntax on

set t_Co=256
"set background=dark
colorscheme dante

"get the mouse going in term (this breaks copy and paste)
"set mouse=a
"set ttymouse=xterm2

"show tabs and trailing whitespace
"need to add

set autoread 
set nomodeline

"write to readonly
cmap w!! w !sudo tee > /dev/null %

"start neredtree
autocmd VimEnter * NERDTree | wincmd p
